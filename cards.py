import deckofcards

# Init the deck and load saved cards if any
doc = deckofcards.DeckOfCards() 

action = ''
while action != 'quit': 

	action = raw_input("What's next? draw(d), reshuffle(r), show(s), quit(q)? ") 
	if action == 'quit' or action == 'q':
		doc.saveState()
		exit()
	elif action == 'draw' or action == 'd':
		doc.draw()
	elif action == 'reshuffle' or action == 'r' :
		doc.reshuffle()
	elif action == 'show' or action == 's' :
		doc.printSortedList()

