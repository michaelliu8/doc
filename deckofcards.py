import json
import requests

class DeckOfCards(object):
    """Desk of Cards. it has the following properties:

    Attributes:
        deck_id: A string representing the id of the deck.
        cardsDrawn: A dictionary tracking the current cards drawn.
    """

    def __init__(self):
    	
        self.statefile = 'deck.json'
        self.urlPrefix = 'https://deckofcardsapi.com/api/deck/'
        self.deck_id = ''
        self.loadState()
        if (self.deck_id == ''):
            response = requests.get(self.urlPrefix + 'new/shuffle/?deck_count=1')
            json_object = response.json()
            self.deck_id  = json_object['deck_id']
            self.cardsDrawn = {}

    def draw(self):

        url = self.urlPrefix + self.deck_id + "/draw/?count=1"
        response = requests.get(url)
        json_object = response.json()
        remaining = json_object['remaining']
        if (json_object['success'] == True):
            print "remaining=" + str(remaining)
            cardValue = json_object['cards'][0]['value']
            print "cardValue=" + str(cardValue)
            if cardValue in self.cardsDrawn:
                # increase count
                self.cardsDrawn[cardValue] = self.cardsDrawn[cardValue] + 1
            else:
                self.cardsDrawn[cardValue] = 1

        else:
            print "draw failed" 

    def reshuffle(self):

        url = self.urlPrefix + self.deck_id  + "/shuffle"
        print "url=" + url
        response = requests.get(url)
        json_object = response.json()
        remaining = json_object['remaining']
        if (json_object['success'] == True):
            print "remaining=" + str(remaining)
            self.cardsDrawn = {}
        else:
            print "reshuffle failed"    

    def saveState(self):
        
        stateObject = {}
        stateObject['deck_id'] = self.deck_id
        stateObject['cards'] = self.cardsDrawn
        with open(self.statefile, 'w') as outfile:
            json.dump(stateObject, outfile)

    def loadState(self):

        try:
            with open(self.statefile) as data_file:
                data_loaded = json.load(data_file)
                print data_loaded
                self.deck_id = data_loaded['deck_id']
                self.cardsDrawn = data_loaded['cards']
                print "your saved cards are loaded, you can continue"
                self.printSortedList()
                print "deck_id=" + self.deck_id
        except IOError:
            pass      
    
    def show(self):

        print "My cards: \n"  + json.dumps(self.cardsDrawn, indent=4)

    def printSortedList(self):

        if "ACE" in self.cardsDrawn:
            print "ACE: " + str(self.cardsDrawn['ACE'])

        for i in range (2,11):
            if str(i) in self.cardsDrawn:
                print  str(i) +  ": " + str(self.cardsDrawn[str(i)])  

        highCards = ['JACK','QUEEN','KING']        
        for hc in highCards:
            if hc in self.cardsDrawn:
                print  hc +  ": " + str(self.cardsDrawn[hc])  
                    
